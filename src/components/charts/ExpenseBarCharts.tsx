import React, { useState } from "react"; 
import { useSelector } from "react-redux";
import ReactApexChart from "react-apexcharts";
import { RootState } from "../../redux/store";
import { ApexOptions } from "apexcharts";
import Button from "@mui/material/Button"; 
import Box from "@mui/material/Box"; 

const ExpenseStackedColumnChart: React.FC = () => {
  const expenses = useSelector((state: RootState) => state.expense.expense);

  const [filter, setFilter] = useState("date"); 


  const uniqueDates = Array.from(new Set(expenses.map((expense: any) => expense.date)));
  const uniqueMonths = Array.from(new Set(expenses.map((expense: any) => expense.date.substr(0, 7))));
  const uniqueYears = Array.from(new Set(expenses.map((expense: any) => expense.date.substr(0, 4))));

  
  let xAxisCategories: string[] = [];
  switch (filter) {
    case "month":
      xAxisCategories = uniqueMonths;
      break;
    case "year":
      xAxisCategories = uniqueYears;
      break;
    default:
      xAxisCategories = uniqueDates;
      break;
  }

  const categories = Array.from(new Set(expenses.map((expense: any) => expense.categories)));

  const seriesData = categories.map((category: any) => ({
    name: category,
    data: xAxisCategories.map((xAxisCategory) => {
      const matchingExpenses = expenses.filter(
        (expense: any) =>
          expense.categories === category &&
          expense.date.startsWith(xAxisCategory)
      );
      const totalExpense = matchingExpenses.reduce(
        (total: number, expense: any) => total + expense.expense,
        0
      );
      return totalExpense;
    }),
  }));

  const chartOptions: ApexOptions = {
    chart: {
      id: "expense-stacked-column-chart",
      stacked: true,
      toolbar: {
        show: false,
      },
    },
    xaxis: {
      categories: xAxisCategories, 
    },
    yaxis: {
      title: {
        text: "Expense Amount",
      },
      labels: {
        formatter: (val: any) =>
          val !== undefined ? parseFloat(val).toFixed(2) : "",
      },
    },
    legend: {
      show: true,
      position: "right",
    },
    plotOptions: {
      bar: {
        horizontal: false,
      },
    },
    tooltip: {
      y: {
        formatter: (val: any) =>
          val !== undefined ? parseFloat(val).toFixed(2) : "",
      },
    },
  };

  return (
    <div>
      <h3>Expense Categories (Stacked Column Chart)</h3>
      <Box display="flex" justifyContent="flex-end" marginBottom="1rem">
        <Button variant={filter === "date" ? "contained" : "outlined"} onClick={() => setFilter("date")}>
          Day
        </Button>
        <Button variant={filter === "month" ? "contained" : "outlined"} onClick={() => setFilter("month")}>
          Month
        </Button>
        <Button variant={filter === "year" ? "contained" : "outlined"} onClick={() => setFilter("year")}>
          Year
        </Button>
      </Box>
      <ReactApexChart
        type="bar"
        options={chartOptions}
        series={seriesData}
        height={350}
        width="100%"
      />
    </div>
  );
};

export default ExpenseStackedColumnChart;
