import { useEffect, useState } from "react";
import "..//.//assets/css/expense.css";
import {
  TextField,
  MenuItem,
  Button,
  FormControl,
  InputLabel,
  Select,
  SelectChangeEvent,
  Grid,
  Paper,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import { useFormik } from "formik";
import * as yup from "yup";
// import CustomTextField from "./common/CustomTextField";
import { useDispatch } from "react-redux";
// import {
//   addexpense,
//   showExpenseForm,
//   totalExpenseInfo,
// } from "../redux/features/expense/expensesSlice";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { useNavigate } from "react-router-dom";
// import CustomTextField from "./common/CustomTextFeild";
import { afterExpenceIncome } from "../redux/features/income/incomeSlice";
import { red } from "@mui/material/colors";
import CustomTextField from "./common/CustomTextField";
import {
  addexpense,
  showExpenseForm,
  totalExpenseInfo,
} from "../redux/features/expense/expenseSlice";
import { addTransaction } from "../redux/features/transaction/transactionSlice";
import {
  ShoppingCart as GroceriesIcon,
  Commute as TransportationIcon,
  LocalActivity as EntertainmentIcon,
  FlashOn as UtilitiesIcon,
  LocalHospital as HealthcareIcon,
  MoreHoriz as OthersIcon,
} from "@mui/icons-material";
// import "../assets/css/expense.css";

const categories: string[] = [
  "Groceries",
  "Transportation",
  "Entertainment",
  "Utilities",
  "Healthcare",
  "Others",
];

const Expenses: React.FC = () => {
  const [totalIncome, setTotalIncome] = useState(0);
  const [totalExpense, setTotalExpense] = useState(0);
  const [error, setError] = useState("");
  // const dispatch = useDispatch()
  const isDialogOpen = useSelector(
    (state: RootState) => state.expense.isDialogOpen
  );
  // const [isDialogOpen, setDialogOpen] = useState(false);
  const navigate = useNavigate();
  const expenseData = useSelector((state: RootState) => state.expense.expense);
  const expenseAmount = useSelector(
    (state: RootState) => state.expense.totalExpense
  );
  const totalincome = useSelector(
    (state: RootState) => state.income.totalIncome
  );
  console.log(totalincome);

  useEffect(() => {
    setTotalIncome(totalincome);
    setTotalExpense(expenseAmount);
  }, [totalincome, expenseAmount]);
  console.log(expenseData);
  const dispatch = useDispatch();
  const validationSchema = yup.object({
    expense: yup
      .number()
      .required("Expense is required")
      .positive("Income must be a positive number"),
    category: yup.string().required("Category is required"),
    customCategory: yup
      .string()
      .test(
        "customTypeRequired",
        'Custom Income Type is required when "Other" is selected',
        function (value) {
          if (this.parent.category === "Others") {
            return !!value;
          }
          return true;
        }
      ),
    date: yup.string().required("Date is required"),
    customNote: yup.string(),
  });

  const handleCloseDialog = () => {
    // Reset the selected date
    dispatch(showExpenseForm(false));
    formik.resetForm();
    setError("");
    // setDialogOpen(false);
  };
  const formik = useFormik({
    initialValues: {
      expense: "",
      category: "",
      customCategory: "",
      date: "",
      customNote: "",
    },
    validationSchema,
    onSubmit: (values) => {
      //   console.log(values);
      const incomeValue = parseInt(values.expense, 10);
      if (totalIncome === 0) {
        setError("You have insufficient balance");
      } else if (totalIncome < incomeValue) {
        setError("you have low balance");
      } else {
        setError("");
        const expenseData = {
          expense: values.expense,
          date: values.date,
          categories:
            values.category === "Others"
              ? values.customCategory
              : values.category,
          customNote: values.customNote ? values.customNote : "--",
        };
        dispatch(addexpense(expenseData));
        dispatch(afterExpenceIncome(expenseData.expense));
        dispatch(totalExpenseInfo(expenseData.expense));
        dispatch(showExpenseForm(false));
        dispatch(addTransaction({ type: "expense", data: expenseData }));
        console.log(expenseData);

        formik.resetForm();
      }
    },
  });

  return (
    <div>
      {/* <p onClick={() => navigate("/expensedetails")}>expense</p> */}
      <Paper elevation={10}>
        <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
          <form onSubmit={formik.handleSubmit}>
            <DialogTitle>Add Expense</DialogTitle>
            <DialogContent>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <FormControl fullWidth margin="normal">
                    <InputLabel>Expense Type</InputLabel>
                    <Select
                      className="select-dropdown"
                      name="category"
                      value={formik.values.category}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    >
                      {categories.map((cat) => (
                        <MenuItem key={cat} value={cat}>
                          {cat === "Groceries" && (
                            <GroceriesIcon className="icon" />
                          )}{" "}
                          {/* Add icons */}
                          {cat === "Transportation" && (
                            <TransportationIcon className="icon" />
                          )}
                          {cat === "Entertainment" && (
                            <EntertainmentIcon className="icon" />
                          )}
                          {cat === "Utilities" && (
                            <UtilitiesIcon className="icon" />
                          )}
                          {cat === "Healthcare" && (
                            <HealthcareIcon className="icon" />
                          )}
                          {cat === "Others" && <OthersIcon />}
                          <span>{cat}</span>
                        </MenuItem>
                      ))}
                    </Select>
                    {formik.touched.category && formik.errors.category && (
                      <Typography color={red[500]}>
                        {formik.errors.category}
                      </Typography>
                    )}
                  </FormControl>
                </Grid>
                {formik.values.category === "Others" && (
                  <Grid item xs={12}>
                    <CustomTextField
                      className=" "
                      label="Custom Category"
                      name="customCategory"
                      type="text"
                      value={formik.values.customCategory}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorText={
                        formik.touched.customCategory &&
                        formik.errors.customCategory
                      }
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <CustomTextField
                    label="Expense"
                    type="number"
                    name="expense"
                    value={formik.values.expense}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    errorText={formik.touched.expense && formik.errors.expense}
                  />
                </Grid>
                <Grid item xs={12}>
                  <CustomTextField
                    label="Date"
                    type="date"
                    name="date"
                    value={formik.values.date}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    errorText={formik.touched.date && formik.errors.date}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <CustomTextField
                    name="customNote"
                    label="Custom Note"
                    value={formik.values.customNote}
                    onChange={formik.handleChange}
                    errorText={
                      formik.touched.customNote &&
                      Boolean(formik.errors.customNote)
                    }
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  {error && <Typography color={red[500]}>{error}</Typography>}
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDialog} color="primary">
                Cancel
              </Button>
              <Button type="submit" variant="contained" color="primary">
                Add
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </Paper>
    </div>
  );
};

export default Expenses;
