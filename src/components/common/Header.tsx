import React from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Menu,
  MenuItem,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import logo from "../../assets/img/logo-square-65a6124237868b1d2ce2f5db2ab0b7c777e2348b797626816400534116ae22d7.svg";
import "../../assets/css/header.css";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { useNavigate, useLocation } from "react-router-dom"; // Import useLocation
import { green, red } from "@mui/material/colors";
import { useDispatch } from "react-redux";
import {logout} from "../../redux/features/userAuth/loginSlice"
const ResponsiveNavbar: React.FC = () => {
  const userData = localStorage.getItem("userData");
  const userLoginData  = userData ? JSON.parse(userData) : null;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.userLogin.login);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));

  // Get the current location
  const location = useLocation();

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar className="appbar">
      <Toolbar>
        <img className="imgProp" src={logo} alt="pass-money" />
        <Typography variant="h6" className="typography">
          Expense Manager
        </Typography>
        {isSmallScreen ? (
          <IconButton
            size="large"
            edge="end"
            color="inherit"
            aria-label="menu"
            onClick={handleMenuOpen}
          >
            <MenuIcon />
          </IconButton>
        ) : (
          <>
            <Button
              color={location.pathname === "/" ? "success" : "inherit"}
              onClick={() => navigate("/")}
            >
              Home
            </Button>
            <Button
              color={location.pathname === "/dashboard" ? "success" : "inherit"}
              onClick={() => navigate("/dashboard")}
            >
              Dashboard
            </Button>
            <Button
              color={location.pathname === "/income" ? "success" : "inherit"}
              onClick={() => navigate("/income")}
            >
              Income
            </Button>
            <Button
              color={location.pathname === "/expense" ? "success" : "inherit"}
              onClick={() => navigate("/expense")}
            >
              Expense
            </Button>
            <Button
              color={
                location.pathname === "/transaction" ? "success" : "inherit"
              }
              onClick={() => navigate("/transaction")}
            >
              Transaction
            </Button>
            {userLoginData ? (
              <Button
                color="inherit"
                onClick={() => {
                  dispatch(logout())
                  navigate("/login");
                  localStorage.removeItem("userData")
                }}
              >
                Logout
              </Button>
            ) : (
              <Button
                color={location.pathname === "/login" ? "success" : "inherit"}
                onClick={() => navigate("/login")}
              >
                Login
              </Button>
            )}
          </>
        )}
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
        >
          <MenuItem
            onClick={() => {
              navigate("/");
              setAnchorEl(null);
            }}
          >
            Home
          </MenuItem>
          <MenuItem
            onClick={() => {
              navigate("/dashboard");
              setAnchorEl(null);
            }}
          >
            Dashboard
          </MenuItem>
          <MenuItem
            onClick={() => {
              navigate("/income");
              setAnchorEl(null);
            }}
          >
            Income
          </MenuItem>
          <MenuItem
            onClick={() => {
              navigate("/expense");
              setAnchorEl(null);
            }}
          >
            Expense
          </MenuItem>
          <MenuItem
            onClick={() => {
              navigate("/transaction");
              setAnchorEl(null);
            }}
          >
            Transaction
          </MenuItem>
          {userLoginData ? (
            <MenuItem onClick={() =>{ dispatch(logout()); navigate("/login"); localStorage.removeItem("userData")}}>Logout</MenuItem>
          ) : (
            <MenuItem onClick={() => navigate("/login")}>Login</MenuItem>
          )}
        </Menu>
      </Toolbar>
    </AppBar>
  );
};

export default ResponsiveNavbar;
