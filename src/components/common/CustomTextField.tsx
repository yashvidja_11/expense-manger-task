import React, { useState } from "react";
import { TextField, InputAdornment, Typography } from "@mui/material";
import { Email, VpnKey, AccountCircle, Visibility, VisibilityOff } from "@mui/icons-material";
import { red } from "@mui/material/colors";
import "../../assets/css/customtextfeild.css"
interface CustomTextFieldProps {
  errorText?: any;
  name?: string;
  type?: string;
  value?: string;
  label?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void;
  InputLabelProps?: any;
  className?: string;
}

const CustomTextField: React.FC<CustomTextFieldProps> = ({
  errorText,
  type,
  name,
  label,
  value,
  onChange,
  onBlur,
  InputLabelProps,
  className,
}) => {
  const [showPassword, setShowPassword] = useState(false);

  let icon;
  let placeholderText;

  if (type === "email") {
    icon = <Email />;
    placeholderText = "Email";
  } else if (type === "password") {
    icon = <VpnKey />;
    placeholderText = "Password";
  } else if (type === "text") {
    icon = <AccountCircle />;
    placeholderText = "Name";
  }

  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  return (
    <>
      <TextField
        className={className}
        name={name}
        type={showPassword ? "text" : type} // Use "text" type when showPassword is true
        label={label}
        placeholder={placeholderText}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        fullWidth
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">{icon}</InputAdornment>
          ),
          endAdornment: type === "password" && (
            <InputAdornment position="end">
              {showPassword ? (
                <VisibilityOff onClick={togglePasswordVisibility} />
              ) : (
                <Visibility onClick={togglePasswordVisibility} />
              )}
            </InputAdornment>
          ),
        }}
        InputLabelProps={InputLabelProps}
      />
      {errorText && (
        <Typography  className="error">
          {errorText}
        </Typography>
      )}
    </>
  );
};

export default CustomTextField;
