import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { Navigate, Outlet } from "react-router-dom";

const ProtectedRoute: React.FC = () => {
  
   const userData = localStorage.getItem("userData");
     const userLoginData  = userData ? JSON.parse(userData) : null;

  if (userLoginData === null) {
    return <Navigate to="/login" />;
  }
  return <Outlet />;
};

export default ProtectedRoute;
