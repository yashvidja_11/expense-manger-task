import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import store from './redux/store'
import { Provider } from 'react-redux'
import MasterLayout from './layout/MasterLayout'
import Hompage from './pages/Hompage'
import Signup from './pages/SignUp'
import Login from './pages/Login'
import ProtectedRoute from './components/ProtectedRoute'
import Dashboard from './pages/Dashboard'
import IncomeDetails from './pages/IncomeDetails'
import Transaction from './pages/Transaction'
import ExpenseDetails from './pages/ExpenseDetails'
import Errorpage from './pages/Errorpage'

const Router : React.FC = () => {
  return (
    <div>
      <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MasterLayout />}>
            <Route index element={<Hompage />} />
            <Route path="signup" element={<Signup />} />
            <Route path="login" element={<Login />} />
            <Route element={<ProtectedRoute />}>
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="income" element={<IncomeDetails />} />
              <Route path="transaction" element={<Transaction />} />
              <Route path="expense" element={<ExpenseDetails />} />
            </Route>
          </Route>
          <Route path="*" element={<Errorpage/>}/>
        </Routes>
      </BrowserRouter>
    </Provider>
    </div>
  )
}

export default Router
