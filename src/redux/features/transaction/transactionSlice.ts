import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface TransactionState {
  transactions: {
    type: "income" | "expense";
    data: {
      income: "string";
      date: "string";
      categories: "string";
      customNote: "string";
    };
  }[];
}

const initialState: TransactionState = {
  transactions: [],
};

const transactionSlice = createSlice({
  name: "transactions",
  initialState,
  reducers: {
    addTransaction: (
      state,
      action: PayloadAction<{ type: "income" | "expense"; data: any }>
    ) => {
      state.transactions.unshift(action.payload);
    },
    afterupdateTransactionData: (state, action) => {
      console.log(action.payload.index);
    },
  },
});
export const { addTransaction, afterupdateTransactionData } =
  transactionSlice.actions;
export default transactionSlice.reducer
