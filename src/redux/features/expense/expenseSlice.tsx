import { createSlice } from "@reduxjs/toolkit";

interface Expense {
  expense: number;
  date: string;
  categories: string;
  customNote: string;
}

interface UserDataState {
  expense: Expense[];
  isDialogOpen: boolean;
  totalExpense: number;
}

const initialState: UserDataState = {
  expense: [],
  isDialogOpen: false,
  totalExpense: 0,
};
const expensesSlice = createSlice({
  name: "expense",
  initialState,
  reducers: {
    addexpense: (state, action) => {
      state.expense.push(action.payload);
    },
    deleteExpense: (state, action) => {
      console.log(action.payload);
      const findExpense = state.expense.find(
        (_, index) => index === action.payload
      );

      console.log(findExpense);
      if (findExpense) {
        state.expense = state.expense.filter(
          (expense) => expense !== findExpense
        );
        state.totalExpense -= findExpense.expense;
      }
    },
    editExpense: (state, action) => {
      const { index, updatedExpense } = action.payload;

      // Find the expense to be updated using the provided index
      const expenseToUpdate = state.expense[index];

      if (expenseToUpdate) {
        // Update the found expense with the properties of updatedExpense
        state.expense[index] = {
          ...expenseToUpdate,
          ...updatedExpense,
        };
      }
    },
    showExpenseForm: (state, action) => {
      state.isDialogOpen = action.payload;
    },
    totalExpenseInfo: (state, action) => {
      state.totalExpense += action.payload;
    },
  },
});

export const {
  addexpense,
  deleteExpense,
  editExpense,
  showExpenseForm,
  totalExpenseInfo,
} = expensesSlice.actions;
export default expensesSlice.reducer;
