import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IncomeState {
  incomes: {
    incomeType: string;
    amount: string;
    date: string;
    customNote: string;
  }[];
  isDialogOpen: boolean;
  totalIncome: number;
}
interface Income {
  incomeType: string,
  amount: string,
  date: string,
  customNote: string

}
const initialState: IncomeState = {
  incomes: [],
  isDialogOpen: false,
  totalIncome: 0,
};


const incomeSlice = createSlice({
  name: "income",
  initialState,
  reducers: {
    addIncome: (state, action) => {
      state.incomes.unshift(action.payload);
    },
    
    deleteIncome: (state, action) => {
      
      state.incomes.splice(action.payload.index, 1);
    
      state.totalIncome -= action.payload.income?.amount
      
    },
    editIncome: (state, action: PayloadAction<{ index: number; updatedData: Income }>) => {
      const { index, updatedData } = action.payload;
      state.incomes[index] = updatedData;
      },

    showIncomeForm: (state, action) => {
      state.isDialogOpen = action.payload;
    },
    totalIncomeIfo: (state, action) => {
      state.totalIncome += action.payload;
    },
    afterExpenceIncome: (state, action) => {
      if(state.totalIncome >0){
        state.totalIncome -= action.payload;
      }
    },
  },
});

export const { addIncome, showIncomeForm, totalIncomeIfo, afterExpenceIncome,deleteIncome ,editIncome } =
  incomeSlice.actions;
export default incomeSlice.reducer;
