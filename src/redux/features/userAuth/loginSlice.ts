import { createSlice } from "@reduxjs/toolkit";
import { start } from "repl";

interface UserDataState {
  login: string;
}

const initialState: UserDataState = {
  login: "",
};
const loginSlice = createSlice({
  name: "loginuser",
  initialState,
  reducers: {
    login: (state, action) => {
      state.login = action.payload;
    },
    logout : (state)=>{
      state.login = ""
    }
  },
});

export const { login , logout} = loginSlice.actions;
export default loginSlice.reducer;
