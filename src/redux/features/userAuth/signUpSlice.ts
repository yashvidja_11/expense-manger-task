import { createSlice } from "@reduxjs/toolkit";

interface UserDataState {
  signUp: { username: string; email: string; password: string }[];
  error: string | null;
}

const initialState: UserDataState = {
  signUp: [],
  error: null,
};
const signUpSlice = createSlice({
  name: "signup",
  initialState,
  reducers: {
    signup: (state, action) => {
      const exisitinguser = state.signUp.find(
        (user) => user.email === action.payload.email
      );
      if (exisitinguser) {
        state.error = "Email already exists";
      } else {
        state.signUp.push(action.payload);
        state.error = null;
      }
    },
  },
});

export const { signup } = signUpSlice.actions;
export default signUpSlice.reducer;
