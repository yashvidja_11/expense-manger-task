import { configureStore } from "@reduxjs/toolkit";
import incomeReducer from '../redux/features/income/incomeSlice'
import signUpSlice from "./features/userAuth/signUpSlice";
import loginSlice from "./features/userAuth/loginSlice";
import transactionSlice from "./features/transaction/transactionSlice"

import expenseSlice from "./features/expense/expenseSlice";
const store = configureStore({
  reducer: {
    userData : signUpSlice,
  userLogin : loginSlice,
    income: incomeReducer,
expense: expenseSlice,
    transaction:transactionSlice
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
